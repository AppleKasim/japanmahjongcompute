//
//  ColorExtension.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/14.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

extension Color {
    static let black3BB6FF = Color("Black3BB6FF")
    static let radioButton = Color("RadioButton")
}
