//
//  StringExtension.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/7/17.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

extension String {
    func getCardTypeContent(comment: String = "") -> String {
        return NSLocalizedString(self, tableName: "CardType", comment: comment)
    }
}


