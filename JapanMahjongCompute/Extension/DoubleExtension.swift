//
//  DoubleExtension.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/22.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

extension Double {
    func ceiling(toInteger integer: Int = 1) -> Double {
        let integer = integer - 1
        let numberOfDigits = pow(10.0, Double(integer))
        return Double(ceil(self / numberOfDigits)) * numberOfDigits
    }
}
