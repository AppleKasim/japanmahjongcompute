//
//  IntExtension.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/22.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

extension Int {
    func ceiling(toInteger integer: Int = 1) -> Int {
        let integer = integer - 1
        let numberOfDigits = pow(10.0, Double(integer))
        return Int(Double(ceil(Double(self) / numberOfDigits)) * numberOfDigits)
    }
}
