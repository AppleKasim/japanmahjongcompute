//
//  ArrayExtension.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/22.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

extension Array where Self.Element == (type: CalculationItemType, value: Int) {
    func map<T>(type: CalculationItemType, interval: Int = 1, transform: (inout Int, Int) -> Void) -> Array<T> {
        map {
            var model = $0
            if model.type == type {
                transform(&model.value, interval)
                if model.value < 0 {
                    model.value = 0
                }
            }
            return model as! T
        }
    }

    func map<T>(type: CalculationItemType, value: Int) -> Array<T> {
        map {
            var model = $0
            if model.type == type {
                model.value = value
            }
            return model as! T
        }
    }
}
