//
//  PointCalculator.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/26.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

struct PointsCalculator {
    static func calculatePoints(appState: AppState) -> String {
        let scoring = appState.scoring.value
        let charm = appState.isSevenPair ? 25 : appState.charm.value
        let treasureCards = appState.treasureCards
        let allScoring = scoring + treasureCards

        // 基本點 ＝ 符 × 2^(番數+2)
        // charm以10為一單位
        let basicPoints = charm * Int(truncating: (pow(2, allScoring + 2)) as NSNumber)
        let overtakePoints = specialCalculate(scoring: allScoring, basicPoints: basicPoints)
        return resultPoint(basicPoints: overtakePoints, isMaster: appState.isMaster)
    }

    private static func specialCalculate(scoring: Int, basicPoints: Int) -> Int {
           switch (scoring, basicPoints) {
           case (let scoring, _)
               where scoring >= 13:
               return 8000
           case (let scoring, _)
               where scoring >= 11:
               return 6000
           case (let scoring, _)
               where scoring >= 8:
               return 4000
           case (let scoring, _)
               where scoring >= 6:
               return 3000
           case (let scoring, let basicPoints)
               where scoring == 5 || basicPoints > 2000:
               return 2000
           default:
                return basicPoints
           }
    }

    private static func resultPoint(basicPoints: Int, isMaster: Bool) -> String {
        let extraText = getExtraText(basicPoints: basicPoints, isMaster: isMaster)

        if isMaster {
            let allPoints = basicPoints * 6
            let personalPoints = (basicPoints * 2).ceiling(toInteger: 3)
            return "\(allPoints.ceiling(toInteger: 3))\n(\(personalPoints))\n\(extraText)"
        } else {
            let allPoints = basicPoints * 4
            let guestPoints = basicPoints.ceiling(toInteger: 3)
            let masterPoints = (basicPoints * 2).ceiling(toInteger: 3)
            return "\(allPoints.ceiling(toInteger: 3)) \n (\(guestPoints), \(masterPoints)) \n \(extraText)"
        }
    }

    /// 滿貫以上的文字顯示
    private static func getExtraText(basicPoints: Int, isMaster: Bool) -> String {
        var extraText = ""

        switch (isMaster, basicPoints) {
        case (true, 8000):
            extraText = "(役滿)"
        case (true, 6000):
            extraText = "(三倍滿)"
        case (true, 4000):
            extraText = "(倍滿)"
        case (true, 3000):
            extraText = "(跳滿)"
        case (true, 2000):
            extraText = "(滿貫)"

        case (false, 8000):
            extraText = "(役滿)"
        case (false, 6000):
            extraText = "(三倍滿)"
        case (false, 4000):
            extraText = "(倍滿)"
        case (false, 3000):
            extraText = "(跳滿)"
        case (false, 2000):
            extraText = "(滿貫)"
        default:
            return ""
        }
        return extraText
    }
}
