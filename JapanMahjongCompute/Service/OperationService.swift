//
//  OperationService.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/5.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

struct OperationService {
    static func getNewAppState(
        _ state: AppState,
        by itemType: CalculationItemType,
        interval: Int = 1,
        transform: (inout Int, Int) -> Void
    ) -> AppState {
        var appState = state
        switch itemType {
        case .scoring:
            transform(&appState.scoring.value, interval)
        case .charm:
            transform(&appState.charm.value, interval)
        case .treasureCards:
            transform(&appState.treasureCards, interval)
        }

        return appState
    }

    static func getNewAppState(
        _ state: AppState,
        by itemType: CalculationItemType,
        value: Int
    ) -> AppState {
        var appState = state
        switch itemType {
        case .scoring:
            appState.scoring.value = value
        case .charm:
            appState.charm.value = value
        case .treasureCards:
            appState.treasureCards = value
        }

        return appState
    }
}
