//
//  NotNegativeNumber.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/5.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

@propertyWrapper
struct NotNegativeNumber {
    var wrappedValue: Int {
        didSet {
            if wrappedValue < 0 {
                wrappedValue = 0
            }
        }
    }
}
