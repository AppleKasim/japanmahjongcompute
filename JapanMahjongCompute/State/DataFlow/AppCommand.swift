//
//  AppCommand.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/15.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation
import Combine

protocol AppCommand {
    func execute(in store: Store)
}


