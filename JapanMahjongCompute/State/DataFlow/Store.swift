//
//  Store.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/15.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Combine
import Foundation

class Store: ObservableObject {
    @Published var appState = AppState()
    
    private var disposeBag = Set<AnyCancellable>()
}

extension Store {
    func dispatch(_ action: AppAction) {
        #if DEBUG
        print("[ACTION]: \(action)")
        #endif
        let result = Store.reduce(state: appState, action: action)
        appState = result.0
        if let command = result.1 {
            #if DEBUG
            print("[COMMAND]: \(command)")
            #endif
            command.execute(in: self)
        }
    }

    static func reduce(state: AppState, action: AppAction) -> (AppState, AppCommand?) {
        var appState = state
        var appCommand: AppCommand? // 對於網路層的應用。目前用不到。

        switch action {
        case .plusTriplet(let itemType, let tripletType):
            appState[keyPath: itemType.keyPath][keyPath: tripletType.keyPath] += 1
            appState.charm.value += itemType.coefficient * tripletType.coefficient

        case .minusTriplet(let itemType, let tripletType):
            let originalValue = appState[keyPath: itemType.keyPath][keyPath: tripletType.keyPath]
            if originalValue > 0 {
                appState[keyPath: itemType.keyPath][keyPath: tripletType.keyPath] -= 1
                appState.charm.value -= itemType.coefficient * tripletType.coefficient
            }

        case .plus(let itemType):
            appState = Store.plus(itemType: itemType, state: state)

        case .minus(let itemType):
            appState = Store.minus(itemType: itemType, state: state)

        case .inputValue(let itemType, let value):
            if let newValue = Int(value) {
                 appState = OperationService.getNewAppState(state, by: itemType, value: newValue)
            }
        case .changeIsMaster(let isMaster):
            appState.isMaster = isMaster
        case .calculate:
            appState.pointsString = PointsCalculator.calculatePoints(appState: appState)
        case .changeReadyHand(let newReadyHand):
            appState = Store.changeReadyHand(newReadyHand: newReadyHand, appState: appState)
        case .changePair(let newPair):
            appState = Store.changePair(newPair: newPair, appState: appState)
        case .changeWindWay(let newWindWay):
            appState = Store.changeWindWay(newWindWay: newWindWay, appState: appState)

        case .changeTriplet(let itemType, let tripletType, let value):
            if let newValue = Int(value) {
                let originalValue = appState[keyPath: itemType.keyPath][keyPath: tripletType.keyPath]
                appState[keyPath: itemType.keyPath][keyPath: tripletType.keyPath] = newValue
                appState.charm.value += (newValue - originalValue) * itemType.coefficient * tripletType.coefficient
            }

        case .set(let isSevenPair):
            appState.isSevenPair = isSevenPair
        }
        
        return (appState, appCommand)
    }
}

extension Store {
    static func changeReadyHand(newReadyHand: ReadyHand, appState: AppState) -> AppState {
        var state = appState
        if newReadyHand == .none, state.readyHand != .none {
            state.charm.value -= state.readyHand.charm
        } else if appState.readyHand == .none, newReadyHand != .none {
            state.charm.value += newReadyHand.charm
        }

        state.readyHand = newReadyHand
        return state
    }

    static func changePair(newPair: Eyes, appState: AppState) -> AppState {
        var state = appState
        switch (newPair, state.eyes) {
        case (.none, _):
            state.charm.value -= state.eyes.charm
        case (_, .none):
            state.charm.value += newPair.charm
        case (_, _)
            where newPair != .none && appState.eyes != .none:
            state.charm.value += newPair.charm - state.eyes.charm
        default:
            break
        }

        state.eyes = newPair
        return state
    }

    static func changeWindWay(newWindWay: WindWay, appState: AppState) -> AppState {
        var state = appState
        switch (newWindWay, state.windWay) {
        case (.none, .concealedHandSelfDrawn):
            state.scoring.value -= state.windWay.scoring
        case (.none, _):
            state.charm.value -= state.windWay.charm
        case (.concealedHandSelfDrawn, _):
            state.scoring.value += newWindWay.scoring
            state.charm.value -= state.windWay.charm
        case (_, .none):
            state.charm.value += newWindWay.charm
        case (_, .concealedHandSelfDrawn)
            where newWindWay != .concealedHandSelfDrawn:
            state.scoring.value -= state.windWay.scoring
            state.charm.value += newWindWay.charm
        case (_, _)
            where newWindWay != .none && state.windWay != .none:
            state.charm.value += newWindWay.charm - state.windWay.charm
        default:
            break
        }

        state.windWay = newWindWay
        return state
    }
}

// MARK: plus and minus
extension Store {
    static func plus(itemType: CalculationItemType, state: AppState) -> AppState {
        let interval = getInterval(itemType: itemType, state: state) {
            $0 == 20 || $0 == 25
        }

        let appState = OperationService.getNewAppState(state, by: itemType, interval: interval, transform: +=)
        return appState
    }

    static func minus(itemType: CalculationItemType, state: AppState) -> AppState {
        let interval = getInterval(itemType: itemType, state: state) {
            $0 == 30 || $0 == 25
        }

        let appState = OperationService.getNewAppState(state, by: itemType, interval: interval, transform: -=)
        return appState
    }

    static func getInterval(itemType: CalculationItemType, state: AppState, exceptionalConditionsForCharm: (Int) -> Bool) -> Int {
        var interval = 1
        if itemType == .charm {
            let value = state.charm.value
            if exceptionalConditionsForCharm(value) {
                interval = 5
            } else {
                interval = 10
            }
        }

        return interval
    }

}
