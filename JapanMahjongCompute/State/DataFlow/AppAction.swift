//
//  AppAction.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/15.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

enum AppAction {
    case plus(itemType: CalculationItemType)
    case minus(itemType: CalculationItemType)
    case inputValue(itemType: CalculationItemType, value: String)
    case changeIsMaster(isMaster: Bool)
    case calculate

    case changeReadyHand(readyHand: ReadyHand)
    case changePair(pair: Eyes)
    case changeWindWay(windWay: WindWay)

    case plusTriplet(itemType: CalculationTripletType, tripletType: TripletType)
    case minusTriplet(itemType: CalculationTripletType, tripletType: TripletType)
    case changeTriplet(itemType: CalculationTripletType, tripletType: TripletType, value: String)

    case set(isSevenPair: Bool)
}
