//
//  AppState.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/15.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation
import Combine

struct AppState {
    var scoring = CalculationModel(type: .scoring, value: 0)
    var charm = CalculationModel(type: .charm, value: 20)

    var tripletMid = TripletModel(type: .tripletMid)
    var tripletSide = TripletModel(type: .tripletSide)

    var readyHand: ReadyHand = .none
    var eyes: Eyes = .none
    var windWay: WindWay = .none

    @NotNegativeNumber
    var treasureCards = 0

    var isSevenPair = false
    var pointsString = "0"
    var isMaster = false
}
