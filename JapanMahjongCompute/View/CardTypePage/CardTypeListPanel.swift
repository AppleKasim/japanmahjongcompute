//
//  CardTypeListPanel.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/7/17.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct CardTypeListPanel: View {
    let list = Bundle.main.decode([CardTypeModel].self, from: "CardTypeList.json")

    var body: some View {
        List {
            ForEach(list) { model in
                Section(header: Text(model.title)) {
                    ForEach(model.content, id: \.self) { item in
                        HStack {
                            Text(item)
                                .multilineTextAlignment(.center)
                                .frame(width: 100.0)

                            RoundedRectangle(cornerRadius: 1)
                                .frame(width: 1)
                                .opacity(0.1)

                            Text(item.getCardTypeContent())
                        }
                    }
                }
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
    
}

struct CardTypeListPanel_Previews: PreviewProvider {
    static var previews: some View {
        CardTypeListPanel()
    }
}
