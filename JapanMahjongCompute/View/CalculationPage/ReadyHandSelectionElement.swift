//
//  ReadyHandSelectionGroup.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/9.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct ReadyHandSelectionElement: View {
    @EnvironmentObject var store: Store

    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("聽牌")

            RadioButton(
                text: "嵌張",
                isChecked: store.appState.readyHand == .midTiles,
                action: .changeReadyHand(readyHand: .midTiles),
                inactive: .changeReadyHand(readyHand: .none)
            )

            RadioButton(
                text: "邊獨",
                isChecked: store.appState.readyHand == .sideTiles,
                action: .changeReadyHand(readyHand: .sideTiles),
                inactive: .changeReadyHand(readyHand: .none)
            )

            RadioButton(
                text: "單騎",
                isChecked: store.appState.readyHand == .singleTiles,
                action: .changeReadyHand(readyHand: .singleTiles),
                inactive: .changeReadyHand(readyHand: .none)
            )
        }
        .padding(.all, 5.0)
        .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 1))
    }
}


struct ReadyHandSelectionElement_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store()
        return ReadyHandSelectionElement().environmentObject(store)
    }
}
