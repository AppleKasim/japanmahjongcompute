//
//  CalculationPanel.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/14.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct CalculationPanel: View {
    @EnvironmentObject var store: Store
    
    var body: some View {
        ScrollView(.vertical) {
            VStack {
                Triplet()
                HStack {
                    ReadyHandSelectionElement()
                    WinWaySelectionElement()

                }

                HStack {
                    PairSelectionElement()
                    CalculationElement(item: .treasureCards, width: 100)
                }

                HStack() {
                    MasterOrGuestView()
                    Spacer()
                    CheckboxField(
                        tag: 1,
                        label: "七將",
                        callback: checkboxSelected
                    )
                }.padding([.leading, .trailing], 12)

                CalculationRegion()
                TotalCalculationButton()
                TextView()

            }
        }
    }

    func checkboxSelected(id: Int, isMarked: Bool) {
        store.dispatch(
            .set(isSevenPair: isMarked)
        )
    }
}

struct CalculationRegion: View {
    var body: some View {
        VStack {

            HStack {
                CalculationElement(item: .scoring)
                CalculationElement(item: .charm)
            }
        }
    }
}

struct Triplet: View {
    var body: some View {
        HStack {
            TripletElement(item: .tripletMid)
            TripletElement(item: .tripletSide)
        }
    }
}

struct TextView: View {
    @EnvironmentObject var store: Store

    var body: some View {
        VStack {
            Text("點數")
            Text(store.appState.pointsString)
                .multilineTextAlignment(.center)
                .fixedSize()
        }
    }
}

struct CalculationPanel_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store()
        return CalculationPanel().environmentObject(store)
    }
}

struct TotalCalculationButton: View {
    @EnvironmentObject var store: Store

    var body: some View {
        Button(
            action: {
                self.store.dispatch(.calculate)
            }, label: {
                Text("計算")
                    .foregroundColor(Color.white)
                    .padding(5)
            })
            .background(Color.black3BB6FF)
            .cornerRadius(5)
    }
}
