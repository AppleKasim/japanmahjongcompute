//
//  CalculationGroup.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/20.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct CalculationElement: View {
    @EnvironmentObject var store: Store
    let item: CalculationItemType
    var width: CGFloat = 180

    var body: some View {
        VStack(spacing: 10) {
            Text(item.text)
                .frame(width: width,alignment: .leading)
            CalculationGroup(item: self.item)
                .frame(width: width)
        }
        .padding(.all, 5.0)
        .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 1))
    }
}

struct CalculationElement_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store()
        return CalculationElement(item: .scoring).environmentObject(store)
    }
}
