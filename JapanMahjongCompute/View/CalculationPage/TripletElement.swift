//
//  TripletGroup.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/9.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct TripletElement: View {
    @EnvironmentObject var store: Store
    let item: CalculationTripletType
    
    var body: some View {
        VStack(spacing: 10) {
            Text(item.text)
                .frame(width: 180,alignment: .leading)

            HStack() {
                Text("明刻子")
                TripletGroup(item: self.item, tripletType: .exposedTriplet)
            }
            HStack() {
                Text("暗刻子")
                TripletGroup(item: self.item, tripletType: .concealedTriplet)
            }
            HStack() {
                Text("明槓子")
                TripletGroup(item: self.item, tripletType: .exposedKong)
            }
            HStack() {
                Text("暗槓子")
                TripletGroup(item: self.item, tripletType: .concealedKong)
            }
        }
        .padding(.all, 5.0)
        .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 1))
    }
}



struct TripletElement_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store()
        return TripletElement(item: .tripletMid).environmentObject(store)
    }
}
