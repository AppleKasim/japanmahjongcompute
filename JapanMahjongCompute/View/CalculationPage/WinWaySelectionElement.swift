//
//  WinWaySelectionGroup.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/9.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct WinWaySelectionElement: View {
    @EnvironmentObject var store: Store

    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("胡牌方式")

            RadioButton(
                text: "門前榮和",
                isChecked: store.appState.windWay == .concealedHandRong,
                action: .changeWindWay(windWay: .concealedHandRong),
                inactive: .changeWindWay(windWay: .none)
            )

            RadioButton(
                text: "自摸(七將不計自摸的2符)",
                isChecked: store.appState.windWay == .selfDrawn,
                action: .changeWindWay(windWay: .selfDrawn),
                inactive: .changeWindWay(windWay: .none)
            )

            RadioButton(
                text: "門前自摸(一番)",
                isChecked: store.appState.windWay == .concealedHandSelfDrawn,
                action: .changeWindWay(windWay: .concealedHandSelfDrawn),
                inactive: .changeWindWay(windWay: .none)
            )
        }
        .padding(.all, 5.0)
        .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 1))
    }
}

struct WinWaySelectionElement_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store()
        return WinWaySelectionElement().environmentObject(store)
    }
}
