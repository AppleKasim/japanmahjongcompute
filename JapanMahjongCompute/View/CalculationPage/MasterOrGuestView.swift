//
//  MasterOrGuestView.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/21.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct MasterOrGuestView: View {
    @EnvironmentObject var store: Store

    var body: some View {
        HStack {
            RadioButton(
                text: "親家",
                isChecked: store.appState.isMaster,
                action: .changeIsMaster(isMaster:true)
            )

            RadioButton(
                text: "子家",
                isChecked: !store.appState.isMaster,
                action: .changeIsMaster(isMaster:false)
            )
        }
    }
}

struct MasterOrGuestView_Previews: PreviewProvider {
    static var previews: some View {
        MasterOrGuestView()
    }
}
