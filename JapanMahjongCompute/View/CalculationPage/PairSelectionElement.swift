//
//  PairSelectionGroup.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/9.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct PairSelectionElement: View {
    @EnvironmentObject var store: Store

    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("雀頭")

            RadioButton(
                text: "自風牌、場風牌、三元牌",
                isChecked: store.appState.eyes == .singleWind,
                action: .changePair(pair: .singleWind),
                inactive: .changePair(pair: .none)
            )

            RadioButton(
                text: "連風牌",
                isChecked: store.appState.eyes == .doubleWind,
                action: .changePair(pair: .doubleWind),
                inactive: .changePair(pair: .none)
            )
        }
        .padding(.all, 5.0)
        .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray, lineWidth: 1))
    }
}

struct PairSelectionElement_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store()
        return PairSelectionElement().environmentObject(store)
    }
}
