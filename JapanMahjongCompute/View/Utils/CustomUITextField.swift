//
//  TextField.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/19.
//  Copyright © 2020 周政翰. All rights reserved.
//

import UIKit
import SwiftUI

struct CustomUITextField: UIViewRepresentable {
    typealias UIViewType = UITextField

    @EnvironmentObject var store: Store
    var itemType: CalculationItemType?
    var tripletItemType: CalculationTripletType?
    var tripletType: TripletType?
    let action: ((_ value: String) -> Void)?
    
    func makeUIView(context: Context) -> UITextField {
        let textField = UITextField()
        textField.delegate = context.coordinator
        textField.keyboardType = .numberPad
        textField.borderStyle = .roundedRect
        textField.textAlignment = .center
        
        return textField
    }

    func updateUIView(_ uiView: UITextField, context: Context) {
        if let itemType = itemType {
            switch itemType {
            case .scoring:
                let value = store.appState.scoring.value
                uiView.text = "\(value)"
            case .charm
                    where store.appState.isSevenPair == true:
                uiView.text = "25"
            case .charm:
                let value = store.appState.charm.value
                uiView.text = "\(value)"
            case .treasureCards:
                let value = store.appState.treasureCards
                uiView.text = "\(value)"
            }
        }

        if let tripletItemType = tripletItemType,
            let tripletType = tripletType {
            switch tripletItemType {
            case .tripletMid:
                let value = store.appState.tripletMid.getValue(by: tripletType)
                uiView.text = value
            case .tripletSide:
                let value = store.appState.tripletSide.getValue(by: tripletType)
                uiView.text = value
            }
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: NSObject, UITextFieldDelegate {
        var parent: CustomUITextField

        init(_ textField: CustomUITextField) {
            self.parent = textField
        }

        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            let isWithinRange = updatedText.count <= Global.textCountLimit
            if isWithinRange {
                parent.action?(updatedText)
            }

            return isWithinRange
        }

        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            let currentText = textView.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }
            let updatedText = currentText.replacingCharacters(in: stringRange, with: text)

            let isWithinRange = updatedText.count <= Global.textCountLimit
            if isWithinRange {
                parent.action?(updatedText)
            }

            return isWithinRange
        }
    }
}

struct CustomUITextField_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store()

        return CustomUITextField(itemType: .scoring, action: nil).environmentObject(store)
    }
}
