//
//  TripletGroup.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/22.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct TripletGroup: View {
    @EnvironmentObject var store: Store
    let item: CalculationTripletType
    let tripletType: TripletType

    var body: some View {
        HStack() {
            CalculationButton(imageName: "minus.square.fill") {
                self.store.dispatch(.minusTriplet(itemType: self.item, tripletType: self.tripletType))
            }
            .frame(width: 30, height: 50)

            CustomUITextField(
                tripletItemType: self.item,
                tripletType: self.tripletType
            ) {
                self.store.dispatch(
                    .changeTriplet(
                        itemType: self.item,
                        tripletType: self.tripletType,
                        value: $0
                    )
                )
            }
            
            CalculationButton(imageName: "plus.square.fill") {
                self.store.dispatch(.plusTriplet(itemType: self.item, tripletType: self.tripletType))
            }
            .frame(width: 30, height: 50)
        }
        .frame(height: 50, alignment: .center)
    }
}

struct TripletGroup_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store()
        return TripletGroup(item: .tripletMid, tripletType: .exposedTriplet).environmentObject(store)
    }
}
