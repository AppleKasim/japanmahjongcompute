//
//  RadioButton.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/21.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct RadioButton: View {
    @EnvironmentObject var store: Store
    
    var text = ""
    var isChecked = false
    var action: AppAction
    var inactive: AppAction? = nil

    var body: some View {
        HStack(spacing: 5) {
            Group {
                if isChecked {
                    Circle()
                        .fill(Color.black3BB6FF)
                        .frame(width: 20, height: 20)
                        .overlay(
                            Circle().fill(Color.radioButton)
                                .frame(width: 8, height: 8)
                        )
                        .onTapGesture {
                            if let inactive = self.inactive {
                                self.store.dispatch(inactive)
                            }
                        }
                } else {
                    Circle()
                        .fill(Color.white)
                        .frame(width: 20, height: 20)
                        .overlay(Circle().stroke(Color.black3BB6FF, lineWidth: 2))
                        .colorMultiply(Color.radioButton)
                        .onTapGesture {
                            self.store.dispatch(self.action)

                    }
                }
            }

            Text(text)
        }
    }
}

struct RadioButton_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store()
        return RadioButton(
            text: "2222",
            action: .changeIsMaster(isMaster: false)).environmentObject(store)
    }
}
