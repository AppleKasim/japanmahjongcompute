//
//  CheckboxField.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/7/14.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

//MARK:- Checkbox Field
struct CheckboxField: View {
    let tag: Int
    let label: String
    let size: CGFloat
    let color: Color
    let textSize: CGFloat
    let callback: (Int, Bool) -> ()

    @State var isMarked: Bool = false

    init(
        tag: Int,
        label:String,
        size: CGFloat = 20,
        color: Color = Color.black,
        textSize: CGFloat = 16,
        callback: @escaping (Int, Bool) -> ()
        ) {
        self.tag = tag
        self.label = label
        self.size = size
        self.color = color
        self.textSize = textSize
        self.callback = callback
    }

    var body: some View {
        Button(action:{
            self.isMarked.toggle()
            self.callback(self.tag, self.isMarked)
        }) {
            HStack(alignment: .center, spacing: 10) {
                Image(systemName: self.isMarked ? "checkmark.square.fill" : "square")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(.black3BB6FF)
                    .frame(width: self.size, height: self.size)

                Text(label)
                    .font(Font.system(size: textSize))
            }.foregroundColor(self.color)
        }
        .foregroundColor(Color.white)
    }
}

struct CheckboxField_Previews: PreviewProvider {
    static var previews: some View {
        CheckboxField(tag: 11, label: "test", callback: {_,_ in

        })
    }
}
