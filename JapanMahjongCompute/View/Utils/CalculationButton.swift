//
//  CalculationButton.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/9.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct CalculationButton: View {
    var imageName = ""
    let action: () -> Void

    var body: some View {
        Button(action: action) {
            Image(systemName: imageName)
                .foregroundColor(.black3BB6FF)
        }
    }
}

struct CalculationButton_Previews: PreviewProvider {
    static var previews: some View {
        CalculationButton(action: {

        })
    }
}
