//
//  CalculationGroup.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/9.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct CalculationGroup: View {
    @EnvironmentObject var store: Store
    let item: CalculationItemType

    var body: some View {
        HStack() {
            CalculationButton(imageName: "minus.square.fill") {
                self.store.dispatch(.minus(itemType: self.item))
            }
            .frame(width: 30, height: 50)

            CustomUITextField(itemType: self.item) {
                self.store.dispatch(.inputValue(itemType: self.item, value: $0))
            }
            CalculationButton(imageName: "plus.square.fill") {
                self.store.dispatch(.plus(itemType: self.item))
            }
            .frame(width: 30, height: 50)
        }
        .frame(height: 50, alignment: .center)
        
    }
}

struct CalculationGroup_Previews: PreviewProvider {
    static var previews: some View {
        let store = Store()
        return CalculationGroup(item: .scoring).environmentObject(store)
    }
}
