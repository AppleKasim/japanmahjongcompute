//
//  CustomTabView.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/7/16.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct CustomTabView: View {
    let store = Store()

    var body: some View {
        TabView {
            CalculationPanel().environmentObject(store)
                .tabItem {
                    Image(systemName: "rectangle.3.offgrid.fill")
                    Text("計算器")
                }

            CardTypeListPanel()
                .tabItem {
                    Image(systemName: "list.dash")
                    Text("役")
                }
        }
    }
}

struct CustomTabView_Previews: PreviewProvider {
    static var previews: some View {
        CustomTabView()
    }
}
