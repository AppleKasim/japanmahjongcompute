//
//  ContentView.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/14.
//  Copyright © 2020 周政翰. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
