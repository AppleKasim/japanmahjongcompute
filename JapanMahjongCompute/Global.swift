//
//  Global.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/20.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

struct Global {
    static let textCountLimit = 3
    static let valueCountLimit = 999
}
