//
//  CalculationItem.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/5/21.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

// MARK: CalculationItemType
enum CalculationItemType {
    case scoring        //番
    case charm          //符
    case treasureCards  //寶牌
}

extension CalculationItemType {
    var text: String {
        switch self {
        case .scoring: return "番"
        case .charm: return "符"
        case .treasureCards: return "寶牌"
        }
    }
}

enum CalculationTripletType {
    case tripletMid     // 面子 中張
    case tripletSide    // 面子 幺九
}

extension CalculationTripletType {
    var text: String {
        switch self {
        case .tripletMid: return "面子 中張"
        case .tripletSide: return "面子 幺九"
        }
    }

    var keyPath: WritableKeyPath<AppState, TripletModel> {
        switch self {
        case .tripletMid: return \AppState.tripletMid
        case .tripletSide: return \AppState.tripletSide
        }
    }

    var coefficient: Int {
        switch self {
        case .tripletMid: return 1
        case .tripletSide: return 2
        }
    }
}

enum TripletType {
    case exposedTriplet     // 明刻子
    case concealedTriplet   // 暗刻子
    case exposedKong        // 明槓子
    case concealedKong      // 暗槓子
}

extension TripletType {
    var text: String {
        switch self {
        case .exposedTriplet: return "明刻子"
        case .concealedTriplet: return "暗刻子"
        case .exposedKong: return "明槓子"
        case .concealedKong: return "暗槓子"
        }
    }

    var keyPath: WritableKeyPath<TripletModel, Int> {
        switch self {
        case .exposedTriplet: return \TripletModel.exposedTriplet
        case .concealedTriplet: return \TripletModel.concealedTriplet
        case .exposedKong: return \TripletModel.exposedKong
        case .concealedKong: return \TripletModel.concealedKong
        }
    }

    var coefficient: Int {
        switch self {
        case .exposedTriplet: return 2
        case .concealedTriplet: return 4
        case .exposedKong: return 8
        case .concealedKong: return 16
        }
    }
}

// MARK: ReadyHand
/// 聽牌
enum ReadyHand {
    case none
    case sideTiles      // 邊獨聽牌
    case midTiles       // 嵌張聽牌
    case singleTiles    // 單騎聽牌
}

extension ReadyHand {
    var charm: Int {
        switch self {
        case .none:
            return 0
        default:
            return 2
        }
    }
}

// MARK: Pair
/// 雀頭
enum Eyes {
    case none
    case singleWind     // 自風牌、場風牌、三元牌
    case doubleWind     // 連風牌
}

extension Eyes {
    var charm: Int {
        switch self {
        case .none:
            return 0
        case .singleWind:
            return 2
        case .doubleWind:
            return 4
        }
    }
}

// MARK: WindWay
/// 胡牌方式
enum WindWay {
    case none
    case concealedHandRong  // 門前榮和
    case selfDrawn          // 自摸
    case concealedHandSelfDrawn   // 門前榮和
}

extension WindWay {
    var charm: Int {
        switch self {
        case .none, .concealedHandSelfDrawn:
            return 0
        case .concealedHandRong:
            return 10
        case .selfDrawn:
            return 2
        }
    }

    var scoring: Int {
        switch self {
        case .concealedHandSelfDrawn:
            return 1
        default:
            return 0
        }
    }
}
