//
//  CardTypeSection.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/7/22.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

struct CardTypeModel: Codable, Identifiable{
    let id = UUID()
    let title: String
    let content: [String]

    enum CodingKeys: String, CodingKey {
        case title
        case content
    }
}
