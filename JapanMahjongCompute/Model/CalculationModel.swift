//
//  CalculationModel.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/1.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

struct CalculationModel {
    let type: CalculationItemType
    
    @NotNegativeNumber
    var value: Int
}
