//
//  TripletModel.swift
//  JapanMahjongCompute
//
//  Created by 周政翰 on 2020/6/17.
//  Copyright © 2020 周政翰. All rights reserved.
//

import Foundation

struct TripletModel {
    let type: CalculationTripletType

    @NotNegativeNumber
    var exposedTriplet = 0

    @NotNegativeNumber
    var concealedTriplet = 0

    @NotNegativeNumber
    var exposedKong = 0

    @NotNegativeNumber
    var concealedKong = 0

    func getValue(by tripletType: TripletType) -> String {
        switch tripletType {
        case .exposedTriplet:
            return "\(exposedTriplet)"
        case .concealedTriplet:
            return "\(concealedTriplet)"
        case .exposedKong:
            return "\(exposedKong)"
        case .concealedKong:
            return "\(concealedKong)"
        }
    }
}
